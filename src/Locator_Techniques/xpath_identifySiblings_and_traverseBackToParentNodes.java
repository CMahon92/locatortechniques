package Locator_Techniques;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class xpath_identifySiblings_and_traverseBackToParentNodes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		//Opens Capital One website
		driver.get("https://www.capitalone.com/");
		
		//traversed from sibling to sibling element using xpath
		//Once complete, Auto link will be clicked on
		driver.findElement(By.xpath("//a[@id='card']/following-sibling::a[2]")).click();
		
		//Traversed from child to parent element using xpath
		//Googled how to loop with Java in order to print web elements in console
		List<WebElement> banner = driver.findElements(By.xpath("//a[@id='unav-l1-logo']/parent::div"));
		for(int i = 0; i< banner.size(); i++) {
	         String coList = banner.get(i).getText();
	         System.out.println("The list of Capital One links are : " + coList);
	      }
		//Located object by using text() for xpath
		//Once located, "Search Cars" will be clicked on
		driver.findElement(By.xpath("//*[text()='Search Cars']")).click();
	}

}
