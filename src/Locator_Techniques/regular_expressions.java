package Locator_Techniques;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class regular_expressions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		//Opens Youtube website
		driver.get("https://www.youtube.com/");
		
		//Created regular expression for xpath
		//purposely did not fully spell search to ensure xpath regular expression worked
		//Used xpath to enter text in the search box
		driver.findElement(By.xpath("//input[contains(@id,'searc')]")).sendKeys("Bunny Binky");
		
		//Created regular expression for CSS
		//Used the value legacy instead of complete id value "search-icon-legacy"
		//Used css to click on the search buttin
		driver.findElement(By.cssSelector("button[id*='legacy']")).click();
		
	}

}
