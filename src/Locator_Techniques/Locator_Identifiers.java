package Locator_Techniques;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Locator_Identifiers {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

	
		System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		//Opens FB website
		driver.get("https://www.facebook.com/");
		
		//locates element by email id; sendKeys is used to enter text in email field
		driver.findElement(By.id("email")).sendKeys("Enter your email");
		
		//locates element by name; sendKeys is used to enter text in password field
		driver.findElement(By.name("pass")).sendKeys("Password");
		
		//When <a> tag is present in HTML, we will use linkText to locate the element
		//.click() clicks on the link
		//driver.findElement(By.linkText("Forgot Password?")).click();
		
		
		//Used xpath to enter mobile number in text field
		driver.findElement(By.xpath("//*[@id=\'identify_email\']")).sendKeys("123-456-7890");
		
		//Used cssSelector to click search button
		driver.findElement(By.cssSelector("#did_submit")).click();
		
		Thread.sleep(8000);
		
		//I used get text to grab the error message once the button is clicked.
		//Error message will print out in console
		//Verified accuracy of xpath in browser console by using $x("")
		System.out.println(driver.findElement(By.xpath("//*[@id=\'identify_yourself_flow\']/div/div[2]/div[1]/div[2]")).getText());
		
		//Generated my own xpath. Verified xpath accuracy in browser console
		//Page will translate to Spanish
		driver.findElement(By.xpath("//a[@title='Spanish']")).click();
		
		//Generated my own css. Received error message when trying to verify css accuracy
		//in browser console. (custom css locator was able to run successfully in eclipse)
		driver.findElement(By.cssSelector("input[id='identify_email']")).sendKeys("Customized css locator");
		
		Thread.sleep(3000);
		driver.quit();
	}

}
