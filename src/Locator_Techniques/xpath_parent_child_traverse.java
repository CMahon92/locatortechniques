package Locator_Techniques;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class xpath_parent_child_traverse {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		//Opens Google website
		driver.get("https://www.google.com/");
		
		//xpath with parent child traverse is useful in the case you may not have a unique attribute (e.g. classname, id,)
		//but still want to identify the xpath
		
		//This parent child xpath will input text in the search box
		driver.findElement(By.xpath("//div[@class='A8SBwf']/div/div/div[2]/input")).sendKeys("i will be clicking the images link");
		
		//This parent child xpath will click the images link 
		driver.findElement(By.xpath("//div[@class='LX3sZb']/div/div/div/div/div[2]/a")).click();
	
	}

}
